MAINFILE = trabalho

all: $(MAINFILE).pdf
	pdflatex 	$(MAINFILE).tex
	bibtex 	$(MAINFILE)
	pdflatex 	$(MAINFILE).tex
	pdflatex 	$(MAINFILE).tex
	pdflatex 	$(MAINFILE).tex

clean:
	rm *.bbl *.aux *.blg *.log *.lot *.lof *.idx || echo "Clean"

view:
	evince trabalho.pdf
